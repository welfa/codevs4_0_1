#include <array>
#define _USE_MATH_DEFINES
#include <cmath>
#include <QCoreApplication>
#include "random"
#include "glwidget.h"
#include "Defs.hpp"
#include "Util.hpp"

using namespace defs;
constexpr int tablenum = 180;
static float sin_table[tablenum];
static float cos_table[tablenum];

void GLWidget::initializeGL(){
    //ヨコ線
    for(int i=0; i<(GridNum+1); i++){
        gridPoints[(i<<2)+0] = 0;                           //srcX
        gridPoints[(i<<2)+1] = i*GridLength;                //srcY
        gridPoints[(i<<2)+2] = GridLength*GridNum;          //dstX
        gridPoints[(i<<2)+3] = i*GridLength;                //dstY
    }
    //タテ線
    for(int i=0; i<(GridNum+1); i++){
        const int gpoff = (GridNum+1)*4;
        gridPoints[gpoff+(i<<2)+0] = i*GridLength;          //srcX
        gridPoints[gpoff+(i<<2)+1] = 0;                     //srcY
        gridPoints[gpoff+(i<<2)+2] = i*GridLength;          //dstX
        gridPoints[gpoff+(i<<2)+3] = GridLength*GridNum;//dstY
    }
    //sincos
    for(int i=0; i<tablenum; i++) {
        sin_table[i] = std::sin((float)i/tablenum*2*M_PI);
        cos_table[i] = std::cos((float)i/tablenum*2*M_PI);
    }
    //マス目線座標のインデックスづくり
    std::iota(gridPointsIndex.begin(), gridPointsIndex.end(), 0);

    makeCurrent();
    glEnable(GL_LINE_SMOOTH);
    qglClearColor(Qt::black);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);
}

void GLWidget::paintGL(){
    glClear(GL_COLOR_BUFFER_BIT);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glColor4f(1.f/1,1.f/1,1.f/1,1.f/10);
    glLineWidth(1.f);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, 0, gridPoints.data());
    glDrawElements(GL_LINES, 2*2*(GridNum+1), GL_UNSIGNED_SHORT, gridPointsIndex.data());
    glDisableClientState(GL_VERTEX_ARRAY);
    //std::cout << "painted" << std::endl;
}

void GLWidget::resizeGL(int width, int height){
    float ratio = width / (float) height;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width, height);

    float paddingW = PaddingH * ratio;
    float xshift = (int)((WinH * ratio)-WinW)/2 + paddingW; //小数部分を抜く
    glOrtho(-xshift, (float)WinW + xshift, (float)WinH + PaddingH, -PaddingH, 5.f, -5.f);

    //std::cout << "resized" << width << "x" << height << " " << WinW << "x" << WinH << std::endl;
}

static inline void drawPoint(float y,float x){
    glBegin(GL_POINTS);
    glVertex2f(x, y);
    glEnd();
}
static void drawCircle(float y, float x, float radius, int id=0) {
    static std::map<int,int> phase;
    constexpr int aida = 6;
    static_assert(tablenum%aida == 0, "bimyo-");
    glBegin(GL_LINES);
    const int lphase = phase[id];
    for(int i=lphase; i<tablenum+lphase; i+=aida) {
        const int idx = i>=tablenum ? i-tablenum : i;
        glVertex2f(cos_table[idx]*radius+x, sin_table[idx]*radius+y);
    }
    phase[id] = (phase[id]+1)%tablenum;
    glEnd();
}

void GLWidget::DrawBoard(CodeRunner::Board* b){
    const float W = 1.f/4;
    const float M = 1.f/2;
    const float S = 1.f/1;
    const float Z = 0.f;
    const float A = 1.f/24;
    const float CP[][6] = {
        {S,S,Z,W, 2,true}, //w
        {Z,Z,S,W, 3,true}, //k
        {W,W,S,W, 3,true}, //f
        {M,Z,M,W, 4,true}, //a
        {W,W,W,S,10,false},//c
        {M,M,Z,M, 6,false},//v
        {M,W,Z,M, 7,false},//b
        {S,Z,Z,S,12,false},//c
        {},                //-
        {Z,M,Z,M, 5,false} //f
    };
    std::random_device rnd;
    std::mt19937 mt(rnd());

    auto ldraw = [&](
            std::vector<CodeRunner::Board::Rectangle>& prect,
            std::vector<CodeRunner::Board::PointUnit>& punit){
        //ユニット効果範囲
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
        for(auto& rect : prect){
            glRectf(
                rect.TopLeft.x*GridLength,
                rect.TopLeft.y*GridLength,
                (rect.BottomRight.x+1)*GridLength,
                (rect.BottomRight.y+1)*GridLength);
        }

        //ユニット
        std::normal_distribution<float> ndist(0.5f, 0.1f);
        glBlendFunc(GL_ONE, GL_ZERO);
        auto getRnd = [&](int i) { return CP[i][5] ? ndist(mt) : 0.5f; };
        for(auto& unit : punit) {
            const int i = (int)unit.utb;
            glColor4f(CP[i][0],CP[i][1],CP[i][2],CP[i][3]);
            glPointSize(CP[i][4]);
            drawPoint(
                    GridLength*(unit.p.y+getRnd(i)),
                    GridLength*(unit.p.x+getRnd(i)));
        } };
    stop_watch<std::chrono::high_resolution_clock> sw;
    this->context()->makeCurrent();
    paintGL();
    glColor4f(W,S,W,A);
    ldraw(b->p1rect, b->p1unit);
    glColor4f(S,W,W,A);
    ldraw(b->p2rect, b->p2unit);

    //資源マス
    glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ONE_MINUS_SRC_COLOR);
    glColor4f(S,S,S,S);
    glLineWidth(2.f);
    const int len = b->rma.size();
    std::normal_distribution<float> ndist(1.f, 0.2f);
    for(int i=0; i<len; i++) {
        auto& point = (*b->resourcepoint)[i];
        auto& strength = b->rma[i];
        drawCircle(
                GridLength*(point.y+0.5f),
                GridLength*(point.x+0.5f),
                (1+strength)<<2,
                i);
    }

    swapBuffers();
    qDebug() << "render:" <<  sw.elapsed<std::chrono::milliseconds>() << endl;
    delete b;
}











