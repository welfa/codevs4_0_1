#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>
#include "Defs.hpp"
#include "CodeRunner.hpp"
#include "Parameters.hpp"

#include <chrono>

class GLWidget : public QGLWidget{
    Q_OBJECT
private:
    std::array<float, 2*4*(defs::GridNum+1)> gridPoints;
    std::array<unsigned short, 2*2*(defs::GridNum+1)> gridPointsIndex;
protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
public:
    explicit GLWidget(QWidget *parent = 0) : QGLWidget(/*QGLFormat(QGL::SampleBuffers),*/parent) {}
signals:
public slots:
    void DrawBoard(CodeRunner::Board*);
};

#endif // GLWIDGET_H
