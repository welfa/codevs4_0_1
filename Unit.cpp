#include <memory>
#include <iterator>
#include <algorithm>
#include <iostream>
#include "Rules.hpp"
#include "Parameters.hpp"
#include "Unit.hpp"
#include "GameManager.hpp"

bool Unit::HasTask(int tid) const {
    for (auto&& task : qtask)
    for (auto&& act : task) {
        if(act->ID == tid)
                return true;
    }
    return false;
}
void Unit::DeleteTask(int tid) {
    for (auto&& task : qtask) {
        task.erase(
            std::remove_if(
                task.begin(),
                task.end(),
                [=](std::shared_ptr<ITaskReserver>& x){return x->ID==tid;}),
            task.end());
    }
}
bool Unit::IsFree() const {
    for (auto&& task : qtask)
        if(!task.empty()) return false;
    return true;
}
void Unit::Free() {
    for (auto&& task : qtask)
        task.clear();
}
Action Unit::Act() {
    type.grow();
    if(!type.isMature())
        return Action::Nothing;
    //TODO:Arrayにして順番が保証されているforeachにする
    //qtaskは明示的にHighから見たい，けど汚い
    UnitAction ua {Action::Nothing, false};
    for(int i=(int)TaskPriority::High; i<(int)TaskPriority::_priority_num; i++){
        if(qtask[i].empty())
            continue;
        ua = qtask[i].front()->Act(*this);
        //ua.actがNothingでなければこのインスタンスは更新されるはず
        //でも結局クライアントはサーバーからのインプットで
        //更新することになるのでいじってない
        if(!ua.cont)
            qtask[i].pop_front();  //継続フラグがfalseならpop
        //actionは1ターンに1回しかできないのでbreak確定
        break;
    }
    return ua.act;
}


//適用だけする．actは呼ぶ側が正しいことを保証する．
CommandResult Unit4Server::ApplyCommand(Action act, int produceid){
    switch (act) {
        case Action::M_Up:    point.MoveUp();    break;
        case Action::M_Down:  point.MoveDown();  break;
        case Action::M_Left:  point.MoveLeft();  break;
        case Action::M_Right: point.MoveRight(); break;
        default:
            UnitTypeBase utb = (UnitTypeBase)((char)act-'0');
            return CommandResult{true,
                Unit4Server (
                        master,
                        produceid,
                        point,
                        Rules::Hp[(int)utb],
                        UnitType(Rules::TurnToMature[(int)utb], utb)) };
    }
    return CommandResult{false, Unit4Server()};
    //移動系ならここでreturnされ，produce系ならswitc文の中でreturnされる
    //ちょいきたない
}
void Unit4Server::Attack(Unit4Server& enemy, int k) const {
    //自分がmatureなら攻撃
    if(type.isMature())
        enemy.hp.Hurt(
                Rules::Damage
                [(int)type.getTypeBase()]
                [(int)enemy.type.getTypeBase()]/k);
}
