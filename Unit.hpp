#ifndef INCLUDED_UNIT
#define INCLUDED_UNIT

#include <queue>
#include <assert.h>
#include <string>
#include <memory>
#include <utility>
#include <algorithm>
#include <deque>
//#include "Parameters.hpp"

class Unit;
class ITaskReserver {
public:
    //検索とかに使うID
    int ID;
    bool operator==(const ITaskReserver& ar) const {return ID == ar.ID;}
    bool operator!=(const ITaskReserver& ar) const {return !(*this==ar);}

    //行動予約用の関数
    //return:Actionとbool:次ターンでも継続して同じactをするか
    //arg(Unit&):this
    virtual UnitAction Act(const Unit&);

    //実装先で状態を好きにもたせられるので，入力無しで永久に固定のActをさせるもよし，
    //パラメータを与えた小さいActを作って必要時に必要分だけ予約するもよしというつもり

    //Actを持ってさえすれば良いのでmapなり戦略集中管理用の
    //クラスへのポインタなり自由に持たせて
};

class Unit{
private:
    typedef std::deque<std::shared_ptr<ITaskReserver>> TaskContainer;
    TaskContainer qtask[3];
protected:
    Master      master;
    int         id;
    Point       point;
    HP          hp;
    UnitType    type;
public:
    Unit(Master master, int id, Point p, int hp, UnitType t) :
        master(master), id(id), point(p), hp(hp), type(t) { }
    Unit() : Unit(Master::Friend, -1, Point(), 0, UnitType(0, (UnitTypeBase)19)) { }

    bool operator==(const Unit& u) const {return id == u.id;}
    bool operator!=(const Unit& u) const {return !(*this==u);}

    //辛く，厳しい
    const Master&   getMaster()     const { return master; }
    //int&にしないとconst{}ダルォ？と言われてしまう，理不尽
          int       getId()         const { return id; }
    const Point&    getPoint()      const { return point; }
    const HP&       getHP()         const { return hp; }
    const UnitType& getUnitType()   const { return type; }

    int SightMht() const {
        return type.isMature() ? Rules::Sight[(int)type.getTypeBase()] : 0;
    }
    int ReachMht() const {
        return type.isMature() ? Rules::Reach[(int)type.getTypeBase()] : 0;
    }

    void Update(Point& p, HP& h, UnitType& t) { point = p; hp = h; type = t; }

    enum class TaskPriority : uint8_t { High=0, Middle, Low, _priority_num };
    void QueTask(TaskPriority wp, const std::shared_ptr<ITaskReserver>& act){
        qtask[(int)wp].push_back(act);
    }
    //idが一致するtaskを全削除
    //同じようなタスクに対して
    //全削除させたくないならidを変えるようにして，
    //全削除させたいならidを同じにして，どうぞ
    //id関係なしに全てのタスクを削除してfreeにするならFree()
    void DeleteTask(int tid);
    bool HasTask(int tid) const;
    bool IsFree() const;
    void Free();

    //strはappendでなく上書きされる 多分変更する
    //1ターン毎に呼ばれる
    Action Act();
};

//以下サーバー向け

struct CommandResult;
class Unit4Server : public Unit {
public:
    Unit4Server () : Unit() { }
    Unit4Server (Master master, int id, Point p, int hp, UnitType t):Unit(master,id,p,hp,t) { }

    CommandResult ApplyCommand(Action act, int produceid);

    //サーバー向け
    //クライアント側はUpdate()して
    void Attack(Unit4Server& enemy, int k) const;
};

struct CommandResult {
    bool isUnitProduced;
    Unit4Server producedUnit;
};

#endif
