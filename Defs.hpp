#ifndef INCLUDED_DEFS
#define INCLUDED_DEFS

namespace defs{
    //GL周り
    constexpr int GridNum = 100;    //一辺のマス目の数
    constexpr int GridLength = 5;   //仕切り線から次の仕切り線の手前までの長さ
    //左右のパディングを含めたステータス表示の幅
    constexpr int PaddingH = 3;
    //statusviewの1Pはx=0より左に描画する
    //WinWはpaddingを含まない幅
    //WinHはpaddingを含まない高さ
    constexpr int WinW = GridNum*GridLength+1,
                  WinH = GridNum*GridLength+1;
}
#endif
