#ifndef INCLUDED_CODERUNNER
#define INCLUDED_CODERUNNER

#include <vector>
#include <QThread>
#include <QProcess>
#include <QMetaType>
#include <QDebug>

#include <mutex>
#include "Rules.hpp"
#include "Parameters.hpp"
#include "GameManager.hpp"


//QtのGUIとGameManagerの橋渡しをする
//ストリームの作成から描画に必要なデータの取り出しまで，
//中間層はすべてコイツ担当
class CodeRunner : public QObject {
    Q_OBJECT
public:
    struct Board {
        struct Rectangle { Point TopLeft; Point BottomRight; };
        struct PointUnit { Point p; UnitTypeBase utb; };
        std::vector<Rectangle> p1rect, p2rect;
        std::vector<PointUnit> p1unit, p2unit;
        const GameManager::ResourcePointArr* resourcepoint;
        GameManager::ResourceMineArr rma;
    };
    struct Progress {
        GameManager::GameInfo info;
        int resource[2];
        uint8_t percentage[2] = {0};
        uint16_t unitNum[2][10] = {{0}};
    };
    struct StageInfo {
        QString name[2];
    };
    const int ID;
    CodeRunner(int gameid, QObject* parent = 0) :QObject(parent), ID(gameid) { }
private:
    Board makeBoard(GameManager& gm, GameManager::ResourceMineArr& rma, Board& board);
    Progress makeProgress(GameManager& gm);
    StageInfo makeStageInfo(GameManager& gm);
public slots:
    void errorcb(QProcess::ProcessError error);
    void finishedcb(int exitcode, QProcess::ExitStatus es);
    void gameStart(
            QProcess *procs,
            std::stringstream *istr,
            std::stringstream *ostr);
signals:
    void DisplayBoard(CodeRunner::Board*);
    //TODO:Progressとともに実装
    void OnGameStart(CodeRunner::StageInfo);
    void OnProgress(CodeRunner::Progress);
    void OnNextStage(GameManager::Winner);
    void OnGameFinish(int);
    void RequireWriteProc();
};

Q_DECLARE_METATYPE(CodeRunner::Board)
Q_DECLARE_METATYPE(CodeRunner::StageInfo)
Q_DECLARE_METATYPE(CodeRunner::Progress)
Q_DECLARE_METATYPE(GameManager::Winner)

//CodeRunnerをThreadでうまく走らせる橋渡しをする．
//所有権をうまくしないとダメ
class CodeRunnerHelper : public QObject {
    Q_OBJECT
private:
    QProcess procs[2];
    QString procCommands[2];
    std::stringstream istr[2], ostr[2];
    //悲しみの2段階入力バッファ
    std::stringstream ibufferstr[2];
    std::mutex mstrtranss[2];
    CodeRunner cr;
    QThread worker;
    void Init();
public:
    const int ID;
    const CodeRunner& GetBody() const { return cr; }
    CodeRunnerHelper(int gameid, QString progCommand[2], QObject* parent = 0)
        : QObject(parent), procCommands{progCommand[0], progCommand[1]},
          cr(gameid), ID(gameid){ Init(); }
    void Start();
    ~CodeRunnerHelper(){ quitWorker(0,QProcess::NormalExit); }
signals:
    void appendInput();
public slots:
    void quitWorker(int exitcode, QProcess::ExitStatus es);
    void WriteProc();
};


#endif
