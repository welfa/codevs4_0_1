#include "mainwindow.h"
#include <iostream>
#include <QApplication>

int main(int argc, char *argv[]) {
    class MyApplication : public QApplication {
        public:
            MyApplication(int argc, char ** argv)
                : QApplication(argc, argv) { }
            //~MyApplication();
        private:
            bool notify(QObject *receiver_, QEvent *event_) {
                try {
                    return QApplication::notify( receiver_, event_ );
                } catch ( std::exception& e ) {
                    //showAngryDialog( e );
                    std::cerr << e.what() << std::endl;
                }
                return false;
            }
    };
    MyApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
