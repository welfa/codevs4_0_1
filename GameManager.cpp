#include <utility>
#include <vector>
#include <random>
#include <unordered_set>
#include <algorithm>
#include <cassert>
#include <thread>
#include <chrono>
#include <chrono>
#include <QCoreApplication>
#include "GameManager.hpp"
#include "Util.hpp"

/*ラムダ式を至るところで使ってるけどほとんどは必然性がない．
 *でもこれより綺麗な書き方が思いつかないのでそのまま．*/

static bool isOK(std::istream& istr) {
    auto currentpos = istr.tellg();
    istr.seekg(-1, istr.end);
    const char c = istr.peek();
    istr.seekg(currentpos);
    return c==' ' || c=='\n';
}
static int waitForData(std::istream& istr) {
    //constなアクセスなのでlock要らなそう
    int len;
    rmvws(istr);
    while((len=strlength(istr))==0) {// || !isOK(istr)) {
        QCoreApplication::processEvents();
        if(istr.bad()) {
            qDebug() << "istream bad occured" << endl;
            return 0;
        }
        rmvws(istr);
    }
    return len;
}

void GameManager::Initialize() {
    Map<bool> used(false);
    std::random_device rnd;
    std::mt19937 mt(rnd());
    auto lplacer = [&](
            double firstdista, double firstdistb,
            int num, int mhtmax, bool isInverted, int range){
        assert(range>0);
        std::vector<Point> place;
        std::gamma_distribution<double> gdist(firstdista,firstdistb);
        for (int i=0; i<num; i++) {
            int x,y;
            do{
                double number = gdist(mt);
                number = std::min(number, (double)mhtmax-1.0);
                std::normal_distribution<double> ndist(number/2, number/3);
                int sa = std::min(std::max((int)ndist(mt),0),(int)number);
                sa = std::min(std::max(sa,0),(int)number);
                y = sa, x = (int)number-y;
                if(isInverted){ //盤面が反転されていたら
                    y = Rules::GridNum - y - 1;
                    x = Rules::GridNum - x - 1;
                }
            } while (used[y][x]);   //既に占領されている場所ならやりなおし
            place.push_back(Point(y,x));    //位置保存
            std::fill(used.begin(y,x,range), used.end(), true);    //資源や城の配置不能場所を記録
        }
        return place;
    };
    //城位置決定
    //資源位置決定
    //パラメータは筋肉
    //autoが使えない
    std::vector<Point> castlep[2] = {
        lplacer(3.2, 4, 1, 40, false, (int)Rules::Sight[(int)UnitTypeBase::Castle]),
        lplacer(3.2, 4, 1, 40, true , (int)Rules::Sight[(int)UnitTypeBase::Castle])
    };
    std::vector<Point> resourcep[2] = {
        lplacer(5, 12, Rules::ResourceNumCounterPart, 99, false, 1),
        lplacer(5, 12, Rules::ResourceNumCounterPart, 99, true , 1)
    };
    {   //資源配置
        int c = 0;
        for(auto& elem : resourcep)
            for (auto& res : elem)
                resourcepoint[c++] = res;
    }
    //城とワーカー5体を追加
    auto laddDefaultUnit = [&](Player& p, Point castlep, Master master) {
        UnitTypeBase utbCastle = UnitTypeBase::Castle;
        UnitTypeBase utbWorker = UnitTypeBase::Worker;
        p.troops[nextId] = Unit4Server(
                master,
                nextId, castlep,
                Rules::Hp[(int)utbCastle],
                UnitType(0, utbCastle));
        p.castle.push_back(nextId);
        nextId++;
        for(int w=0; w<Rules::DefaultWorkerNum; w++) {
            p.troops[nextId] = Unit4Server(
                master,
                nextId, castlep,
                Rules::Hp[(int)utbWorker],
                UnitType(0, utbWorker));
            nextId++;
        }
    };
    laddDefaultUnit(p1,castlep[0][0],Master::P1);
    laddDefaultUnit(p2,castlep[1][0],Master::P2);
}
void GameManager::RegistPlayerName() {
    auto lregistname = [&](Player& p) {
        //初回は名前を読む
        DecodeName(p.input, p.name);
        qDebug() << "read name:" << p.name.c_str() << endl;
    };
    lregistname(p1);
    lregistname(p2);
}
void GameManager::CommandPhase() {
    auto lcommand = [&](Player& p, Player& o){
        const std::string nl = "\n";
        p.output
            << 300000/*p.timelimit.count()*/ << nl
            << info.Stage << nl
            << info.Turn << nl
            << p.resource << nl
            << p.troops.size() << std::endl;//nl;
        for (auto& it : p.troops)
            EncodeCommandSC(p.output, it.second);

        //視界を記録
        Map<bool> sight(false);
        for (auto& it : p.troops) {
            Unit4Server& u = it.second;
            std::fill(sight.begin(u.getPoint(),u.SightMht()), sight.end(), true);
        }
        {   //敵ユニットの追加
            int enemycount = 0;
            std::stringstream strenemy;
            for (auto& oit : o.troops) {
                const Point& oup = oit.second.getPoint();
                if(!sight[oup])
                    continue;
                EncodeCommandSC(strenemy, oit.second);
                ++enemycount;
            }
            p.output
                << enemycount << nl
                << strenemy.str();  //nlいらない
            //.str()が無いと動かない．p.outputがただのistreamだからか・・
        }
        {   //資源マスの追加
            int resourcecount = 0;
            std::stringstream strresource;
            for (auto& rp : resourcepoint) {
                if(!sight[rp])
                    continue;
                resourcecount++;
                strresource << rp.y << ' ' << rp.x << nl;
            }
            p.output
                << resourcecount << nl
                << strresource.str();  //nlいらない
        }
        p.output << "END" << std::endl;
    };
    lcommand(p1,p2);
    lcommand(p2,p1);
}

void GameManager::ActionPhase(){
    //入力を受け取る
    //正しい行動か判定する
    //状態に適用する
    auto laction = [&](Player& p) {
        int linenum = -1;
        waitForData(p.input);
        p.input >> linenum;
        assert(!p.input.fail());
        assert(linenum>=0);
        if(linenum>(int)p.troops.size())
            assert(false && "linenum exceeds troop size"); //return;
        std::unordered_set<int> usedId;
        for(int line = 0; line < linenum; line++) {
            int id = -1;
            Action act;
            bool isSuccess = DecodeCommandCS(p.input, id, act);
            if(!isSuccess || usedId.count(id)>0) {
                //assert(false && "duplicated id");//continue; //不正〜〜
                qDebug() << "duplicated id" << id;
                continue;
            }
            usedId.insert(id);  //使用済みID記録
            try {
                Unit4Server& u = p.troops.at(id);
                if(!IsValidCommand(u, act, &fencemap, p.resource)){
                    qDebug() << "invalid command. id:" << id << endl;
                    continue;
                }
                CommandResult cr = u.ApplyCommand(act, nextId);
                //ユニットが生産されていたら続く処理を行う
                if(!cr.isUnitProduced)
                    continue;
                //追加
                Unit4Server& newbee = cr.producedUnit;
                UnitTypeBase utbnb = newbee.getUnitType().getTypeBase();
                assert(nextId == newbee.getId());
                //資源を消費する
                p.resource -= Rules::Cost[(int)utbnb];
                //新入りを格納
                //newbeeは u4s& だけどこの書き方正しい？
                //moveコンストラクタが呼べれば何でも良くない？
                //std::mapは無いkeyを渡すと追加したvalueへの参照を返す
                p.troops[nextId] = std::move(newbee);
                //newbeeがfenceならfence値を増やす
                //城なら記録する
                switch(utbnb) {
                    case UnitTypeBase::Fence:
                        ++fencemap[newbee.getPoint()];
                        break;
                    case UnitTypeBase::Castle:
                        p.castle.push_back(newbee.getId());
                        break;
                    default: break;
                }
                nextId++;
            } catch (const std::out_of_range& oor) {
                qDebug()
                    << "oor:" << p.name.c_str()
                    << " invalid id:" << id << endl;
            }
        }
    };
    laction(p1);
    laction(p2);
}

void GameManager::CombatPhase(){
    auto lcombat = [&](Player& p, Player& o) {
        //敵ユニットのマップを1からつくる
        typedef std::vector<int> enemymapvec;
        Map<enemymapvec> enemymap;
        enemymap.Clear(enemymapvec());
        for (const auto& oit : o.troops) {
            const Unit4Server& ou = oit.second;
            try {
                enemymap[ou.getPoint()].push_back(ou.getId());
            } catch (const std::exception& e) {
                assert(false && "oshimai");
            }
        }
        //各自ユニットについてkを計算し戦わせる
        //kのキャッシュ.0は未計算で k=要素の値-1
        //意味がアレだけどここでしか使わないし勘弁
        static_assert(
            Rules::Sight[0]==Rules::Sight[1] &&
            Rules::Sight[0]==Rules::Sight[2] &&
            Rules::Sight[0]==Rules::Sight[3],
                "kcache does not work properly");
        Map<uint16_t> kcache(0);
        for (const auto& pit : p.troops) {
            const Unit4Server& pu = pit.second;
            const Point& pup = pu.getPoint();
            int k = 0;
            //リーチにいる敵ユニットを数え上げてkを計算
            bool suitcache = pu.SightMht() == Rules::Sight[(int)UnitTypeBase::Worker];
            if(suitcache && kcache[pup]!=0) {
                k = kcache[pup] - 1;
            } else {
                for_each(enemymap.begin(pup, pu.SightMht()), enemymap.end(),
                        [&](const enemymapvec& masuenemy){
                        k += std::min((int)masuenemy.size(), 10); });
                if(suitcache)
                    kcache[pup] = k + 1;
            }
            //敵ユニットを攻撃させる
            if(k==0) continue;  //枝刈り
            for_each(enemymap.begin(pup, pu.SightMht()), enemymap.end(),
                [&](enemymapvec& _masuenemy) {
                    for (int enemyid : _masuenemy) {
                        assert(k!=0 && "attempt to attack nothing");
                        pu.Attack(o.troops[enemyid], k);
                    }});
        }
    };
    lcombat(p1,p2);
    lcombat(p2,p1);
}
void GameManager::EliminatePhase(){
    auto leliminate = [&](Player& p) {
        //vectorの追加削除をしながらrange_based_for
        //for(:)が使えないのが悲しかった（小並）
        for(auto it = p.troops.begin(); it != p.troops.end();) {
            auto& pu = (*it).second;
            if(!pu.getHP().isDead()) {
                ++it;
                continue;
            }
            //puは削除対象
            switch ((UnitTypeBase)pu.getUnitType().getTypeBase()) {
                case UnitTypeBase::Fence:   fencemap[pu.getPoint()]--;      break;
                case UnitTypeBase::Castle:  p.castle.remove(pu.getId());    break;
                default: break;
            }
            p.troops.erase(it++);
        }
    };
    leliminate(p1);
    leliminate(p2);
}

GameManager::ResourceMineArr GameManager::MinePhase(){
    ResourceMineArr arr = {0};
    auto lmine = [&](Player& p){
        auto lcacheResourceRemain = [&](GameInfo gi) {
            static Map<int8_t> resourceRemain;
            static GameInfo sgi = {114,514};
            if(gi.Stage != sgi.Stage) {
                resourceRemain.Clear(0);
                for (auto& rp : resourcepoint) {
                    resourceRemain[rp] = (uint8_t)Rules::MineOut;
                }
                sgi = gi;
            }
            return resourceRemain;
        };
        //残り資源数
        Map<int8_t> resourceRemain = lcacheResourceRemain(info);
        //ワーカを探して掘らせる
        //軍の数はタカがしれてるから毎ターン探索してもね．多少はね．
        for (auto& pit : p.troops) {
            Unit4Server& pu = pit.second;
            const Point& pup = pu.getPoint();
            //資源枯れてないかつワーカー
            if(resourceRemain[pup] > 0 &&
                pu.getUnitType().getTypeBase() == UnitTypeBase::Worker){
                p.resource += Rules::MineEachTurnPerWorker;
                resourceRemain[pup]--;
            }
        }
        //城1つずつに与えられる分
        p.resource += Rules::MineEachTurnPerCastle * p.castle.size();
        //掘り量のログとり
        for (int i=0; i<Rules::ResourceNumCounterPart*2; i++) {
            arr[i] += (uint16_t)Rules::MineOut - resourceRemain[resourcepoint[i]];
        }
    };
    lmine(p1);
    lmine(p2);
    return arr;
}
GameManager::Winner GameManager::TerminatePhase(){
    Winner winner = Winner::Continue;
    //少なくとも片方が死んでいたor時間切れの場合の勝敗
    auto ldeadjudge = [](const Player& p) {
        //城の数が0 or タイムリミット
        return p.castle.empty() || p.timelimit.count() <= 0;
    };
    if(ldeadjudge(p2))
        winner = Winner::P1;
    if(ldeadjudge(p1)) {
        if(winner == Winner::Continue)
            winner = Winner::P2;
        else
            winner = Winner::Draw;
    }
    if(winner != Winner::Continue)
        return winner;  //ターン数をカウントアップせずに終了
    //ターン数制限の勝敗
    info.Turn++;
    if(info.Turn >= Rules::MaxTurn)
        return Winner::Draw;
    //全ての条件に引っかからなかったら
    return Winner::Continue;
}


void GameManager::DecodeName(std::istream& istr, std::string& name) {
    constexpr int maxnamelen = 128;
    char cname[maxnamelen+2] = {};
    //stringに読み込ませると名前の後も全部引っこ抜くのでダメなときがある？
    //TODO:\nはスキップできるようにする
    assert(!istr.fail());
    waitForData(istr);
    rmvws(istr);
    istr.getline(cname, maxnamelen+1);
    if(istr.fail()) {
        qDebug() << "Max length of player name is " << maxnamelen;
    }
    name = cname;
    //istr >> name;
    assert(name.length()>0);
    assert(!istr.fail());
}
void GameManager::EncodeCommandCS(
        std::ostream& ostr,
        int id,
        Action a) {
    constexpr char sep = ' ';
    if(a==Action::Nothing)
        return;
    ostr << id << sep << (char)a << '\n';
}
bool GameManager::DecodeCommandCS(
        std::istream& istr,
        int& id,
        Action& act) {
    char ctmp = 0xFF;
    assert(!istr.fail());

    waitForData(istr);
    istr >> id;
    //waitForData(istr);
    istr >> std::ws;
    istr >> ctmp;
    //qtextstreamクンは'0'がくるとctmpに0を入れる
    if(0<=ctmp && ctmp<=9)
        ctmp += '0';
    act = (Action)ctmp;

    assert(!istr.fail());
    if(id>=0 && '0'<=ctmp && ctmp<='Z')
        return true;
    else
        return false;
}
void GameManager::EncodeCommandSC(
        std::ostream& ostr,
        const Unit4Server& u) {
    constexpr char sep = ' ';
    const Point& pnt = u.getPoint();
    ostr
        << u.getId() << sep
        << pnt.y << sep
        << pnt.x << sep
        << (int)(u.getHP()) << sep
        << (unsigned int)(u.getUnitType()) << std::endl;// << '\n';
}
void GameManager::DecodeCommandSC(
        std::istream& istr,
        int& id,
        Point& pnt,
        HP& hp,
        UnitType& ut) {
    int y,x,ihp;
    unsigned int iut;
    assert(!istr.fail());
    waitForData(istr);
    istr >> id;
    //waitForData(istr);
    istr >> y;
    //waitForData(istr);
    istr >> x;
    //waitForData(istr);
    istr >> ihp;
    //waitForData(istr);
    istr >> iut;
    pnt = Point(y,x);
    hp = HP(ihp);
    ut = UnitType(iut);
    assert(!istr.fail());
}

