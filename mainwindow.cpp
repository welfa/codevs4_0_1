#include <utility>
#include <QSettings>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "glwidget.h"
#include "CodeRunner.hpp"
#include "GameManager.hpp"

const QString iniFileName("settings.ini");

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    qRegisterMetaType<CodeRunner::Board*>("CodeRunner::Board*");
    qRegisterMetaType<CodeRunner::StageInfo>("CodeRunner::StageInfo");
    qRegisterMetaType<CodeRunner::Progress>("CodeRunner::Progress");
    qRegisterMetaType<GameManager::Winner>("GameManager::Winner");
    qRegisterMetaType<QProcess*>("QProcess*");
    qRegisterMetaType<std::stringstream*>("std::stringstream*");
    qRegisterMetaType<QProcess::ExitStatus>("QProcess::ExitStatus");
    qRegisterMetaType<QProcess::ProcessError>("QProcess::ProcessError");
    //qRegisterMetaType<std::stringstream>("stringstream");


    qDebug() << "thread id=" << QThread::currentThreadId();
}

MainWindow::~MainWindow() {
    delete ui;
}
void MainWindow::showEvent(QShowEvent *event) {
    static bool first = true;
    //reentrantじゃなさそうなので適当
    if(!first) return;
    first = false;
    //TODO:取ってつけたような設定取得保存をなおす
    QLineEdit* lines[2] = {ui->filename_P1, ui->filename_P2};
    QSettings settings( iniFileName, QSettings::IniFormat );
    settings.beginGroup("Commands");
    QString params[2] = {
        settings.value("P1").toString(),
        settings.value("P2").toString()
    };
    settings.endGroup();
    for(int i=0; i<2; i++)
        lines[i]->setText(params[i]);
}

void MainWindow::closeEvent(QCloseEvent *event) {
    //デストラクタがよしなにしてくれる
    game.clear();

    QLineEdit* lines[2] = {ui->filename_P1, ui->filename_P2};
    QSettings settings( iniFileName, QSettings::IniFormat );
    settings.beginGroup("Commands");
    settings.setValue("P1", lines[0]->text());
    settings.setValue("P2", lines[1]->text());
    settings.endGroup();
}

void MainWindow::on_Runstop_Button_clicked() {
    QString progCommands[2]
        = { ui->filename_P1->text(),
            ui->filename_P2->text()};
    std::random_device rd;
    unsigned int gameid = rd();
    game.emplace(
            std::piecewise_construct,
            std::forward_as_tuple(gameid),
            std::forward_as_tuple(gameid, progCommands));
    auto& crh = game.at(gameid);
    connect(&crh.GetBody(), SIGNAL(DisplayBoard(CodeRunner::Board*)),
            ui->openGLWidget, SLOT(DrawBoard(CodeRunner::Board*)));
    connect(&crh.GetBody(), SIGNAL(OnGameStart(CodeRunner::StageInfo)),
            this, SLOT(onGameStart(CodeRunner::StageInfo)));
    connect(&crh.GetBody(), SIGNAL(OnProgress(CodeRunner::Progress)),
            this, SLOT(onProgress(CodeRunner::Progress)));
    connect(&crh.GetBody(), SIGNAL(OnNextStage(GameManager::Winner)),
            this, SLOT(onNextStage(GameManager::Winner)));
    connect(&crh.GetBody(), SIGNAL(OnGameFinish(int)),
            this, SLOT(onGameFinish(int)));
    crh.Start();
    //"copyはできない"とエラーになる
    //game.insert(std::make_pair(gameid, CodeRunnerHelper(gameid, progCommands)));
    ui->Runstop_Button->setEnabled(false);
}

void MainWindow::onGameStart(CodeRunner::StageInfo si) {
    static QLabel* const name[2] = {ui->Name_P1, ui->Name_P2};
    assert(si.name[0].length()>0);
    for(int i=0; i<2; i++) {
        name[i]->setText(si.name[i]);
    }
}
void MainWindow::onProgress(CodeRunner::Progress p) {
    static QProgressBar* const strengthBar[2] = {ui->progressBar_P1, ui->progressBar_P2};
    static QLCDNumber* const unitNumCounter[2][10] = { {
        ui->Worker_P1,   ui->Knight_P1,   ui->Fighter_P1,  ui->Assassin_P1, ui->Castle_P1,
        ui->Village_P1,  ui->Base_P1,     ui->Chimera_P1,  nullptr,         ui->Fence_P1
        }, {
        ui->Worker_P2,   ui->Knight_P2,   ui->Fighter_P2,  ui->Assassin_P2, ui->Castle_P2,
        ui->Village_P2,  ui->Base_P2,     ui->Chimera_P2,  nullptr,         ui->Fence_P2
        } };
    static QProgressBar* const unitBalancer[] = {
        ui->Worker_Balance,   ui->Knight_Balance,   ui->Fighter_Balance,  ui->Assassin_Balance, ui->Castle_Balance,
        ui->Village_Balance,  ui->Base_Balance,     ui->Chimera_Balance,  nullptr,              ui->Fence_Balance
    };
    static QLCDNumber* const resourceDisplay[2] = {ui->Resource_P1, ui->Resource_P2};

    ui->stageturn->setText("Stage:" + QString::number(p.info.Stage) + " Turn:" + QString::number(p.info.Turn));
    //if(QCoreApplication::hasPendingEvents())
    //    return;

    int rsum = 0;
    for(int i=0; i<2; i++) {
        strengthBar[i]->setValue(p.percentage[i]);
        resourceDisplay[i]->display(p.resource[i]);
        rsum += p.resource[i];
    }
    ui->Resource_Balance->setValue(rsum!=0 ? 100.0*p.resource[0]/rsum : 50);
    for(int j=0; j<10; j++) {
        if(unitNumCounter[0][j] == nullptr) continue;
        int sum = 0;
        for(int i=0; i<2; i++) {
            unitNumCounter[i][j]->display(p.unitNum[i][j]);
            sum += p.unitNum[i][j];
        }
        unitBalancer[j]->setValue(sum!=0 ? 100.0*p.unitNum[0][j]/sum : 50);
    }
}
void MainWindow::onNextStage(GameManager::Winner winner) {
    static QLabel* const katiboshi[2] = {ui->Info_P1, ui->Info_P2};
    static const QString katistar("★");
    static const QString drawstar("☆");

    assert(winner != GameManager::Winner::Continue && "invalid result");
    QString star[2];
    switch (winner) {
        case GameManager::Winner::P1:   star[0] = katistar;             break;
        case GameManager::Winner::P2:   star[1] = katistar;             break;
        case GameManager::Winner::Draw: star[0] = star[1] = drawstar;   break;
        //書かないとうるさい
        case GameManager::Winner::Continue:                             break;
    }
    for(int i=0; i<2; i++) katiboshi[i]->setText(katiboshi[i]->text() + star[i]);
}
void MainWindow::onGameFinish(int gameid) {
    static QLabel* const katiboshi[2] = {ui->Info_P1, ui->Info_P2};
    static QLabel* const name[2] = {ui->Name_P1, ui->Name_P2};
    //game.erase(gameid);
    //危ないのでしない
    ui->Runstop_Button->setEnabled(true);
    printlog("Game Finished.\n");
    //TODO:雑
    printlog("Winner:" + (katiboshi[0]->text().length()>katiboshi[1]->text().length() ? name[0]->text() : name[1]->text()));
}

void MainWindow::printlog(QString str){
    if(str.isNull())
        return;
    ui->Log_Text->appendPlainText(str);
}
