#ifndef INCLUDED_MAP
#define INCLUDED_MAP

#include <iterator>
#include <algorithm>
#include <array>
#include <assert.h>
#include <limits>
#include "Rules.hpp"
#include "Parameters.hpp"

template<typename T> class MapIterator;

//TODO:T=boolの時にメモリを食わないよう
//  x要素はコンテナを指定可能にしてvectorを使えるように
template<typename T>
class Map {
private:
    typedef std::array<T, Rules::GridNum> yelem_t;
    //T map[][]
    std::array<yelem_t, Rules::GridNum> map;
    //T map[Rules::GridNum][Rules::GridNum];
public:

    Map() { }
    void Clear(T v) {
        for (auto& var : map)
            std::fill(std::begin(var), std::end(var), v);
    }
    Map(T defaultValue) {
        Clear(defaultValue);
    }
    //Map[y][x]が可能になる
    //iteratorに添字を使うとT&が返ってくる
    typename Map<T>::yelem_t::iterator operator[](int y) {
        assert(y>=0 && y<Rules::GridNum);
            return map[y].begin();
    }
    T& operator[](Point p) {
        assert(p.x>=0 && p.x<Rules::GridNum);
        return ((*this)[p.y])[p.x];
        //return map[p.y][p.x];
    }

    MapIterator<T> begin(Point centerp, int mhtDist) {
        return MapIterator<T>(this, centerp, mhtDist);
    }
    MapIterator<T> begin(int centery, int centerx, int mhtDist) {
        return begin(Point(centery, centerx), mhtDist);
    }
    MapIterator<T> end() const {
        static const auto end = MapIterator<T>();
        return end;
    }
};

///http://ameblo.jp/nana-2007-july/entry-10089815465.html
template<typename T>
class MapIterator : public std::iterator<std::forward_iterator_tag, T> {
private:
    constexpr static int invalid = std::numeric_limits<int>::min();
    Map<T>* const map;
    const int cx, cy, fmd;    //初期x,y,終わりのmanhattan dist
    //TODO:(x,y)をPointにする
    int x, y, md;       //次のx,y,mahhattan dist
    enum direction { NE, NW, SW, SE } dir; //(cx,cy)から見て今いる方角．進む向きじゃない
    bool isValid() const { return 0<=x && 0<=y && x<Rules::GridNum && y<Rules::GridNum; }
    bool isTerminated() const { return fmd==invalid || md>fmd; }
public:
    MapIterator(Map<T>* m, Point center, int mhtDist)
        : map(m), cx(center.x), cy(center.y), fmd(mhtDist),
          x(cx), y(cy), md(0), dir(SE) {
        assert( mhtDist>=0 && "invalid mhtDist" );
    }
    //defaultでよくない？
    MapIterator(const MapIterator<T>& copy)
        : map(copy.map), cx(copy.cx), cy(copy.cy), fmd(copy.fmd),
          x(copy.x), y(copy.y), md(copy.md), dir(copy.dir) { }
    MapIterator()
        : map(nullptr), cx(-1), cy(-1), fmd(invalid) { }

    Point GetPoint() const { return Point(y, x); }

    T& operator *() /*const*/ {
        return (*map)[y][x];
        //return (*map)[Point(y,x)];
    }
    bool operator ==(const MapIterator& mi) const {
        if(isTerminated() && mi.isTerminated())
            return true;    //どっちも終了状態(end)のとき
        return false;
    }
    bool operator !=(const MapIterator& mi) const {
        return !(mi == *this);
    }

    MapIterator<T>& operator ++(){
        do{ //terminatedな状態で突入したら状態は未定義だけどすぐ抜ける
            switch (dir) {
                //ダメなときにfall throughすることで
                //中心から次に行く時の処理の特殊化を回避
                case NE: if(x!=cx) { y--, x--; break; } else { dir=NW; }
                case NW: if(y!=cy) { y++, x--; break; } else { dir=SW; }
                case SW: if(x!=cx) { y++, x++; break; } else { dir=SE; }
                case SE: if(y!=cy) { y--, x++; break; } else { dir=NE; x++; md++; break; }
                default: assert(false && "invalid direction");
            }
            //意味が取りにくいので条件を分けた
            if(isTerminated()) break;
        } while(!isValid());
        return *this;
    };

    MapIterator<T> operator ++(int) {
        MapIterator<T> tnp(*this);
        ++(*this);
        return tnp;
    };

};

#endif
