#-------------------------------------------------
#
# Project created by QtCreator 2015-03-01T16:33:04
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = codevs4_0_1
TEMPLATE = app
CONFIG += c++11

SOURCES += main.cpp\
    mainwindow.cpp \
    glwidget.cpp \
    CodeRunner.cpp \
    GameManager.cpp \
    Unit.cpp


HEADERS  += mainwindow.h \
    glwidget.h \
    CodeRunner.hpp \
    Defs.hpp \
    Parameters.hpp \
    GameManager.hpp \
    Rules.hpp \
    Map.hpp \
    Unit.hpp \
    Util.hpp

FORMS    += mainwindow.ui
