#ifndef INCLUDED_RULES
#define INCLUDED_RULES

#include <stdint.h>
#include <chrono>
#include "Defs.hpp"

namespace Rules {
    constexpr int GridNum = defs::GridNum;
    constexpr std::chrono::minutes DefaultTimelimits(5);
    constexpr int MaxTurn = 1000;
    //マップの片側に存在するクリスタルの数
    constexpr int ResourceNumCounterPart = 10;
    //一方の軍がひとつの資源から1ターン毎に得られる最大資源数
    constexpr int MineOut = 5;
    //資源マス上の1ワーカーが1ターンで得られる資源数
    constexpr int MineEachTurnPerWorker = 1;
    //1城が1ターンで得られる資源数
    constexpr int MineEachTurnPerCastle = 10;
    //初期城と一緒に置かれるワーカーの数
    constexpr int DefaultWorkerNum = 5;
    //初期資源数
    constexpr int DefaultResources = 50;
    //便宜上というか仕様上，Sightのマンハッタン距離0は中心座標だけ""見える""
    constexpr int Sight[] =
    {    4,    4,    4,    4,   10,   10,    4,   10,   -1,    0};
/*       w,    k,    f,    a,    c,    v,    b,    c,    -,    f*/
    constexpr int Reach[] =
    {    2,    2,    2,    2,   10,    2,    2,    4,   -1,    0};
/*       w,    k,    f,    a,    c,    v,    b,    c,    -,    f*/
    constexpr int Cost[] =
    {   40,   20,   40,   60, 5000,  100,  500,15000,   -1,   50};
/*       w,    k,    f,    a,    c,    v,    b,    c,    -,    f*/
    constexpr int Hp[] =
    { 2000, 5000, 5000, 5000,50000,20000,20000,50000,    0, 2000};
/*       w,    k,    f,    a,    c,    v,    b,    c,    -,    f*/
    constexpr uint8_t TurnToMature[] =
  //{    0,    0,    0,    0,   16,    8,    8,    8,    0,    0};
    {    0,    0,    0,    0,    0,    0,    0,    0,    0,    0};
/*       w,    k,    f,    a,    c,    v,    b,    c,    -,    f*/
    //fenceは0じゃないとサーバの実装上辛い

    constexpr int Damage[10][10] = {
/*       w,    k,    f,    a,    c,    v,    b,    c,    -,    f*/
    {  100,  100,  100,  100,  100,  100,  100,  100,   -1,  100},//w
    {  100,  500,  200,  200,  200,  200,  200, 1000,   -1,  200},//k
    {  500, 1600,  500,  200,  200,  200,  200,  500,   -1,  300},//f
    { 1000,  500, 1000,  500,  200,  200,  200,  500,   -1,  200},//a
    {  100,  100,  100,  100,  100,  100,  100,  100,   -1,  100},//c
    {  100,  100,  100,  100,  100,  100,  100,  100,   -1,  100},//v
    {  100,  100,  100,  100,  100,  100,  100,  100,   -1,  100},//b
    {  500,  500,  500,  500,20000,  100,  100,  500,   -1,  200},//c
    {0},
    {    0,    0,    0,    0,    0,    0,    0,    0,   -1,    0},//f
    };

    constexpr bool ValidCommand[15][10] = {
/*       w,    k,    f,    a,    c,    v,    b,    c,    -,    f*/
    { true, true, true, true,false,false,false, true,false,false},//U
    { true, true, true, true,false,false,false, true,false,false},//D
    { true, true, true, true,false,false,false, true,false,false},//L
    { true, true, true, true,false,false,false, true,false,false},//R
    {false,false,false,false, true, true,false,false,false,false},//0w
    {false,false,false,false,false,false, true,false,false,false},//1k
    {false,false,false,false,false,false, true,false,false,false},//2f
    {false,false,false,false,false,false, true,false,false,false},//3a
    { true,false,false,false,false,false,false,false,false,false},//4c
    { true,false,false,false,false,false,false,false,false,false},//5v
    { true,false,false,false,false,false,false,false,false,false},//6b
    {false,false,false,false, true,false,false,false,false,false},//7c
    {false,false,false,false,false,false,false,false,false,false},//8 
    { true,false,false,false,false,false,false,false,false,false},//9f
    {false}//invalid command
    };
};
#endif
