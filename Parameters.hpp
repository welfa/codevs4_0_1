#ifndef INCLUDED_PARAMETERS
#define INCLUDED_PARAMETERS

#include <stdint.h>

class Point{
public:
    constexpr static int16_t Undefined = -1;
    int16_t y,x;
    //和,差の計算とかするだろうのでy,xの範囲は使用者の責任にする
    Point() :Point(Undefined,Undefined) { }
    Point(int16_t sy, int16_t sx) :y(sy), x(sx) { }
    Point* MoveUp()      { y--; return this; }
    Point* MoveDown()    { y++; return this; }
    Point* MoveLeft()    { x--; return this; }
    Point* MoveRight()   { x++; return this; }

    Point operator+(const Point& p) const { return Point(y+p.y, x+p.x); }
    Point operator-(const Point& p) const { return Point(y-p.y, x-p.x); }
    bool operator==(const Point& p) const { return y==p.y && x==p.x; }
    bool operator!=(const Point& p) const { return !(*this==p); }
    Point& operator+=(const Point& p) { y+=p.y; x+=p.x; return *this; }
    Point& operator-=(const Point& p) { y-=p.y; x-=p.x; return *this; }
    static Point Fix(Point p) {
        return Point(
            p.y<0 ? 0 : p.y>=Rules::GridNum ? Rules::GridNum-1 : p.y,
            p.x<0 ? 0 : p.x>=Rules::GridNum ? Rules::GridNum-1 : p.x);
    }
};

class HP{
public:
    constexpr static int Undefined = -1;
    int hp;

    HP() :HP(Undefined) { }
    HP(int shp) :hp(shp) { }
    operator int() const { return hp; }
    int Hurt(int damage) {
        hp-=damage;
        if(hp<0) hp=0;
        return hp;
    }
    bool isDead() const { return hp<=0; }
};

enum class Master : uint8_t {
    P1      = 0,
    P2      = 1,
    Friend  = 0,
    Foe     = 1
};

//unionで管理しようとしたけどパディングが環境依存っぽいのでやめた
enum class UnitTypeBase : uint8_t{
    Worker    =  0,
    Knight    =  1,
    Fighter   =  2,
    Assassin  =  3,
    Castle    =  4, //城
    Village   =  5, //村
    Base      =  6, //拠点
    Chimera   =  7, //キマイラ
    _reserved =  8,
    Fence     =  9  //フェンス．視界は通るが敵軍は超えられない
};
class UnitType{
private:
    constexpr static int utbbitband = 8;
    constexpr static unsigned int mask = (1<<utbbitband) - 1;

    UnitTypeBase utb;
    uint8_t turnToMature;

    void init(unsigned int v){
        utb = (UnitTypeBase)(v & mask);
        turnToMature = (uint8_t)(v >> utbbitband);
    }

public:
    UnitType(unsigned int tid){ init(tid); }
    UnitType(uint8_t ttm, UnitTypeBase utb) : utb(utb), turnToMature(ttm)  { }
    UnitType& operator =(unsigned int value) { init(value); return *this; }
    explicit operator unsigned int() const {
        return ((unsigned int)turnToMature<<utbbitband) | ((unsigned int)utb);
    }

    UnitTypeBase getTypeBase() const { return utb; }
    int getTurnToMature() const { return turnToMature; }
    bool isMature() const { return turnToMature==0; }
    void grow() { if(turnToMature>0) --turnToMature; }
    bool isBuilding() const {
        switch (utb) {
            case UnitTypeBase::Castle:
            case UnitTypeBase::Village:
            case UnitTypeBase::Base:
            case UnitTypeBase::Fence:
                return true;
            default:
                return false;
        }
    }
    bool isArmy() const {
        switch (utb) {
            case UnitTypeBase::Knight:
            case UnitTypeBase::Fighter:
            case UnitTypeBase::Assassin:
            case UnitTypeBase::Chimera:
                return true;
            default:
                return false;
        }
    }
    bool isWorker() const { return utb==UnitTypeBase::Worker; }
};

enum class Action : char {
    Nothing     = ' ',
    M_Up        = 'U',
    M_Down      = 'D',
    M_Left      = 'L',
    M_Right     = 'R',
    P_Worker    = ('0'+(uint8_t)UnitTypeBase::Worker),
    P_Knight    = ('0'+(uint8_t)UnitTypeBase::Knight),
    P_Fighter   = ('0'+(uint8_t)UnitTypeBase::Fighter),
    P_Assassin  = ('0'+(uint8_t)UnitTypeBase::Assassin),
    P_Castle    = ('0'+(uint8_t)UnitTypeBase::Castle),
    P_Village   = ('0'+(uint8_t)UnitTypeBase::Village),
    P_Base      = ('0'+(uint8_t)UnitTypeBase::Base),
    P_Chimera   = ('0'+(uint8_t)UnitTypeBase::Chimera),
    P_Fence     = ('0'+(uint8_t)UnitTypeBase::Fence),
};
struct UnitAction {
    Action  act;
    bool    cont;
};

#endif
