#ifndef INCLUDED_MANAGER
#define INCLUDED_MANAGER

#include <map>
#include <chrono>
#include <exception>
#include <stdexcept>
#include <list>
#include <limits>
#include <sstream>
#include <istream>  //いる？
#include <ostream>
#include <string>
#include <QDebug>
#include "Rules.hpp"
#include "Parameters.hpp"
#include "Map.hpp"
#include "Unit.hpp"


class GameManager {
public:
    struct Player {
        Master master;
        std::string name;
        std::chrono::microseconds timelimit;
        //Unit4Serverの実体はこいつが持つ
        //ポインタ取得するなidを取得しろ(いかり)
        std::map<int, Unit4Server> troops;
        //HP表示やらでなにかと使う 実体はtroopsの中
        std::list<int> castle;
        int resource;
        bool summonChimera;
        std::istream& input;
        std::ostream& output;
        Player(Master m, std::istream& input, std::ostream& output) :
            master(m),
            name("uninitialized"),
            timelimit(Rules::DefaultTimelimits),
            resource(Rules::DefaultResources),
            summonChimera(false),
            input(input),
            output(output) { }
        void Reset() {
            timelimit = Rules::DefaultTimelimits;
            troops.clear();
            castle.clear();
            resource = Rules::DefaultResources;
            summonChimera = false;
        }
    };
    struct GameInfo {
        int16_t Stage;
        int16_t Turn;
        GameInfo(int16_t s=0, int16_t t=0):Stage(s), Turn(t) { }
    };
    typedef std::array<Point, Rules::ResourceNumCounterPart*2> ResourcePointArr;
    typedef std::array<uint16_t, Rules::ResourceNumCounterPart*2> ResourceMineArr;
private:
    //資源マス 2つとも同じ内容
    ResourcePointArr resourcepoint;
    //フェンス
    Map<int> fencemap;
    //次に割り当てられるID
    int nextId;

    Player p1,p2;   //2要素を配列にする必要無いしこっちのが見やすいと思う
    GameInfo info;

public:
    GameManager() = delete;
    GameManager(std::istream* istr[2], std::ostream* ostr[2]) :
        fencemap(0), nextId(0),
        p1(Master::P1, *istr[0], *ostr[0]),
        p2(Master::P2, *istr[1], *ostr[1]) { }

    //ゲーム進行の必要処理は下の7つで全て行う
    enum class Winner : uint8_t { Continue, P1, P2, Draw };
    void Initialize();
    void RegistPlayerName();
    void CommandPhase();
    void ActionPhase();
    void CombatPhase();
    void EliminatePhase();
    ResourceMineArr MinePhase();
    Winner TerminatePhase();

    void NextStage() {
        info.Turn = 0;
        info.Stage++;
        p1.Reset();
        p2.Reset();
        //resourcepointは後で上書きされる
        fencemap.Clear(0);
        nextId = 0;
    }

    const Player& GetPlayer(Master m) const {
        switch (m) {
            case Master::P1: return p1; break;
            case Master::P2: return p2; break;
        default:    throw std::range_error("p1 ka p2 (ikari)");
        }
    }
    const GameInfo& GetInfo() const { return info; }
    const ResourcePointArr& GetResourcePoint() const { return resourcepoint; }

    //TODO:各ユニットのセンテンス1行だけじゃなくて行数把握から全ユニット読み込みまでこなす
    static void DecodeName      (std::istream& istr, std::string& name);
    static void EncodeCommandCS (std::ostream& ostr, int id, Action a);
    static bool DecodeCommandCS (std::istream& istr, int& id, Action& act);
    static void EncodeCommandSC (std::ostream& ostr, const Unit4Server& u);
    static void DecodeCommandSC (std::istream& istr,
            int& id, Point& p, HP& hp, UnitType& ut);

    //Unitは信頼できるパラメータ，actは間違えているかもしれないパラメータ
    //blockedmapにif(value)で評価可能なMap<typeof(value)>を入れておくと
    //通れるか通れないかまで判定する．falseなら通れる
    //resourceに値を入れておくと生産コマンドでは資源があるか判定する
    template<typename M = int>
    static bool IsValidCommand(
            Unit& u,
            Action act,
            Map<M>* blockedmap = nullptr,
            int resource = std::numeric_limits<int>::max()) {
        const char cact = (char)act;
        //Nothingは"発行できるコマンド"ではない
        //成熟していないユニットは共通してどんなコマンドも受け付けない
        if(act == Action::Nothing || !u.getUnitType().isMature())
            return false;

        //移動コマンドの場合，それが境界外でないか
        Point p(act==Action::M_Up ? -1 : act==Action::M_Down ? 1 : 0,
                act==Action::M_Left ? -1 : act==Action::M_Right ? 1 : 0);
        p += u.getPoint();
        if(p.y<0 || p.y>=Rules::GridNum || p.x<0 || p.x>=Rules::GridNum)
            return false;

        //移動先が移動不可能でないか
        //Mapに番兵を置いて検知できればかっこいいけどMapがカオスになるので今はパス
        if(blockedmap!=nullptr) {
            if((*blockedmap)[p]) return false;
        }
        //資源不足でないか
        if(('0'<=cact && cact<='9') && resource < Rules::Cost[cact-'0'])
            return false;

        //存在しないコマンド,許可されてないコマンドはNG
        return Rules::ValidCommand
            [('0'<=cact && cact<='9') ? cact-'0'+4 :
                act == Action::M_Up ? 0 :
                act == Action::M_Down ? 1 :
                act == Action::M_Left ? 2 :
                act == Action::M_Right ? 3 : 14]
            [(int)u.getUnitType().getTypeBase()];
    }

};

#endif
