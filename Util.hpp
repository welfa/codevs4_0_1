#ifndef INCLUDED_UTIL
#define INCLUDED_UTIL

template<typename C>
class stop_watch {
    std::chrono::time_point<C> start;
    public:
    stop_watch() : start(C::now()) {}
    template<typename U>
    typename U::rep elapsed() const {
        return std::chrono::duration_cast<U>(C::now() - start).count();
    }
    void reset() { start = C::now(); }
};

template<typename T>
inline bool mabiki(const T value, const int a) { return !(value&(a-1)); }

inline void rmvws(std::istream& istr) {
    assert(!istr.fail());
    istr >> std::ws;
    istr.clear();
}
inline int strlength(std::istream& istr) {
    const auto currentpos = istr.tellg();
    istr.seekg(0, istr.end);
    int len = istr.tellg() - currentpos;
    istr.seekg(currentpos);
    return len;
}
#endif
