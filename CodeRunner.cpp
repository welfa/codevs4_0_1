#include <limits>
#include <sstream>
#include "CodeRunner.hpp"
#include "GameManager.hpp"
#include "Util.hpp"

void CodeRunner::gameStart(
        QProcess *procs,
        std::stringstream *istr,
        std::stringstream *ostr) {
    //p*proc(ref)
    //TODO:リファクタリング
    QProcess &p1proc = procs[0], &p2proc = procs[1];
    qDebug() << "gamestart threadid:" <<  QThread::currentThreadId();

    std::istream* tmpistr[2] = {&istr[0], &istr[1]};
    std::ostream* tmpostr[2] = {&ostr[0], &ostr[1]};
    GameManager gm(tmpistr, tmpostr);
    gm.RegistPlayerName();

    while(gm.GetInfo().Stage < 5 && p1proc.state() && p2proc.state()){
        GameManager::Winner winner;
        gm.Initialize();
        emit OnGameStart(makeStageInfo(gm));
        do {
            qDebug() << "Turn" << gm.GetInfo().Turn;
            gm.CommandPhase();
            emit RequireWriteProc();
            stop_watch<std::chrono::high_resolution_clock> sw;
            gm.ActionPhase();
            qDebug() << "action" << sw.elapsed<std::chrono::milliseconds>(); sw.reset();
            gm.CombatPhase();
            gm.EliminatePhase();
            auto rma = gm.MinePhase();
            winner = gm.TerminatePhase();

            //描画処理とか
            if(mabiki(gm.GetInfo().Turn, 4)) {
                emit OnProgress(makeProgress(gm));
            }
            if(mabiki(gm.GetInfo().Turn, 1)) {
                Board* b = new Board();
                makeBoard(gm, rma, *b);
                emit DisplayBoard(b);
            }
            if(!p1proc.state() || !p2proc.state()) {
                qDebug() << "otita" << endl;
                break;  //落ちたな
            }
        } while (winner == GameManager::Winner::Continue);
        //勝者がきまったのでどうにかしろ
        emit OnProgress(makeProgress(gm));
        emit OnNextStage(winner);
        gm.NextStage();
    }
    //TODO:上にやらせる
    p1proc.terminate();
    p2proc.terminate();
    emit OnGameFinish(ID);
}

CodeRunner::Board CodeRunner::makeBoard(GameManager& gm, GameManager::ResourceMineArr& rma, Board& board) {
    auto lmaker = [&](Master m) {
        const GameManager::Player& p = gm.GetPlayer(m);
        auto& vrect = m==Master::P1 ? board.p1rect : board.p2rect;
        auto& vpoiu = m==Master::P1 ? board.p1unit : board.p2unit;
        //mapiterator使いたかったけどなんか整合性が取れないので筋肉
        for (const auto& it : p.troops) {
            const Unit4Server& pu = it.second;
            const Point& point = pu.getPoint();
            const UnitTypeBase utb = pu.getUnitType().getTypeBase();
            const int sight = pu.SightMht();
            vpoiu.push_back({point, utb});
            for(int y=0; y<=sight; y++) {
                Point dy(y,0), dx(0,sight-y);
                vrect.push_back({
                        Point::Fix(point-dy-dx),
                        Point::Fix(point+dy+dx)});
            }
        }
    };
    lmaker(Master::P1);
    lmaker(Master::P2);
    board.rma = rma;
    board.resourcepoint = &gm.GetResourcePoint();
    return board;
}
void CodeRunner::errorcb(QProcess::ProcessError error){
    qDebug() << "Process Error!:" << error;
};
void CodeRunner::finishedcb(int exitcode, QProcess::ExitStatus es){
    qDebug() << "Process Exit!:" <<
        "code=" << exitcode << " status=" << es;
}
CodeRunner::StageInfo CodeRunner::makeStageInfo(GameManager& gm) {
    StageInfo si;
    si.name[0] = gm.GetPlayer(Master::P1).name.c_str();
    si.name[1] = gm.GetPlayer(Master::P2).name.c_str();
    return si;
}
CodeRunner::Progress CodeRunner::makeProgress(GameManager& gm) {
    Progress p;
    p.info = gm.GetInfo();
    for(int i=0; i<2; i++) {
        const auto& player = gm.GetPlayer(i==0 ? Master::P1 : Master::P2);
        p.resource[i] = player.resource;
        {
            int sum = 0;
            for(const int cuid : player.castle) { sum += player.troops.at(cuid).getHP(); }
            p.percentage[i]
                = (int)(100.0 * sum / player.castle.size() / Rules::Hp[(int)UnitTypeBase::Castle]);
        }
        for(const auto& pair : player.troops) {
            const auto& u = pair.second;
            p.unitNum[i][(int)u.getUnitType().getTypeBase()]++;
        }
    }
    return p;
}

void CodeRunnerHelper::Init() {
    for(int i=0; i<2; i++) {
        QProcess& proc           = procs[i];
        std::stringstream& isstr = istr[i];
        std::stringstream& ibuf  = ibufferstr[i];
        std::mutex& mstrtrans    = mstrtranss[i];

        proc.setProcessChannelMode(QProcess::SeparateChannels);
        proc.setInputChannelMode(QProcess::ManagedInputChannel);
        proc.setReadChannel(QProcess::StandardOutput);
        auto lcallback = [&] {
            bool isappend = false;
            while(proc.bytesAvailable() > 0) {
                //.data()はポインタしか返さないので値を保持する
                const auto gotstr = proc.readAllStandardOutput();
                const auto* data = gotstr.data();
                const int size = gotstr.size();
                assert(!ibuf.fail());
                //isstr.write(data, size);
                mstrtrans.lock();
                ibuf.write(data, size);
                mstrtrans.unlock();
                assert(!ibuf.fail());
                //qDebug() << "output:" << (void*)(&isstr) << gotstr.data();
                isappend = true;
            }
            //qDebug() << "error:" << proc.readAllStandardError();
            if(isappend)
                emit appendInput();
        };
        //中間バッファを設けてスレッド同士のストリーム読み書きの衝突を防いでいる
        //こっちのスレッド:qprocssから読む->ibufに書き込む
        //あっちのスレッド:(同期的に)ibufからisstrに移す
        //ココに全てを詰め込むことでまともなlockを達成
        //以前はblockingqueuedconnectionで2スレッド間で同期を取ってたのでかなりマシ
        //あとはGameManagerの新入力待ちに使うqEventﾅﾝﾁｬﾗをlambdaで渡せば完全分離達成
        qDebug() << "output1 connect:" <<
            connect(&proc, &QProcess::readyReadStandardOutput, lcallback);
                    //this , lcallback,
                    //Qt::DirectConnection);
                    ////Qt::BlockingQueuedConnection);
        qDebug() << "output2 connect:" <<
            connect(this , &CodeRunnerHelper::appendInput,
                    &cr  , [&](){
                        assert(!isstr.fail());
                        assert(!ibuf.fail());
                        if(strlength(ibuf)>0) {
                            mstrtrans.lock();
                            isstr << ibuf.rdbuf() << std::flush;
                            ibuf.str(std::string());
                            ibuf.clear();
                            mstrtrans.unlock();
                            assert(!isstr.fail());
                            assert(!ibuf.fail());
                        }
                    }, Qt::QueuedConnection);
        qDebug() << "input connect:" <<
            connect(&cr  , SIGNAL(RequireWriteProc()),
                    this ,   SLOT(WriteProc()));
        qDebug() << "error connect:" <<
            connect(&proc, SIGNAL(error(QProcess::ProcessError)),
                    &cr  ,   SLOT(errorcb(QProcess::ProcessError)));
        qDebug() << "finish connect:" <<
            connect(&proc, SIGNAL(finished(int, QProcess::ExitStatus)),
                    &cr  ,   SLOT(finishedcb(int, QProcess::ExitStatus)));
        qDebug() << "finish quit connect:" <<
            connect(&proc, SIGNAL(finished(int, QProcess::ExitStatus)),
                    this ,   SLOT(quitWorker(int, QProcess::ExitStatus)));
    }
}
void CodeRunnerHelper::Start() {
    auto lprocstart = [&](QProcess& proc, QString& command) {
        const QString nl("\n");
        proc.start(command, QIODevice::ReadWrite);
        qDebug() << "process" << proc.processId() << "started";
        if(!proc.waitForStarted()) {
            throw std::runtime_error("bad execute command.\n"); //雑
        }
        qDebug()
            << "isOpen:" << proc.isOpen() << nl
            << "isSequential:" << proc.isSequential() << nl
            << "isReadable:" << proc.isReadable() << nl
            << "isWritable:" << proc.isWritable();
    };
    cr.moveToThread(&worker);
    worker.start();
    QMetaObject::invokeMethod(
            &cr, "gameStart",
            Qt::AutoConnection,
            Q_ARG(QProcess*, procs),
            Q_ARG(std::stringstream*, istr),
            Q_ARG(std::stringstream*, ostr));
    lprocstart(procs[0], procCommands[0]);
    lprocstart(procs[1], procCommands[1]);
}

void CodeRunnerHelper::quitWorker(int exitcode, QProcess::ExitStatus es) {
    static std::mutex terminator;
    qDebug() << "quit worker!\nExit Status:" << es << "Exit code:" << exitcode;
    if(terminator.try_lock()) {
        if(worker.isRunning()) { worker.terminate(); worker.wait(); }
        for(int i=0; i<2; i++) {
            if(procs[i].state() != QProcess::NotRunning) {
                procs[i].terminate();
                if(!procs[i].waitForFinished(5000))
                    qDebug() << "チーン";
            }
        }
        terminator.unlock();
    }
}
void CodeRunnerHelper::WriteProc() {
    for(int i=0; i<2; i++) {
        QProcess& proc = procs[i];
        std::stringstream& osstr = ostr[i];
        assert(!osstr.fail());
        auto wstr = osstr.str();
        if(wstr.length()<=0) {
            qDebug() << "nazo call";
            continue;
        } else {
            qDebug() << "yoi call";
        }
        if(proc.write(wstr.data(), wstr.length()) != (int)wstr.length() ||
                !proc.waitForBytesWritten()) {
            assert(false && "failed to write command\n");
        }
        //clear
        osstr.str("");
        osstr.clear();  //clear error state
    }
}
