#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <map>
#include "CodeRunner.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_Runstop_Button_clicked();
    void onGameStart(CodeRunner::StageInfo);
    void onProgress(CodeRunner::Progress);
    void onNextStage(GameManager::Winner);
    void onGameFinish(int);

private:
    void closeEvent(QCloseEvent *event);
    void showEvent(QShowEvent *event);
    Ui::MainWindow *ui;
    std::map<int, CodeRunnerHelper> game;
    void printlog(QString str);
};

#endif // MAINWINDOW_H
